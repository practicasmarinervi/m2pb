<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_7</title>
    </head>
    <body>
        <?php
        function dibujarCirculo($x,$y){
        echo "<circle cx = \"$x\" cy = \"$y\" r = \"50\" fill =\"" . calculoColor() . "\" />";
        }

        function calculoColor() {
            $color = "rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")";
            return $color;
        }

        $color = calculoColor();
        ?>
        <p>Color: <?= $color ?></p>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="1000px" height="1000px"
             style="display: block;margin: 0px auto;">
        <?php
        dibujarCirculo(50, 50);
        dibujarCirculo(100, 50);
        dibujarCirculo(200, 250);
        ?>

        </svg>
    </body>
</html>