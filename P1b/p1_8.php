<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_8</title>
        <script type="text/javascript">

            /*
             *Modificar para que cuando usuario pinche en un circulo desde JS se
             oculte ese circulo (utilizar display de CSS)
             */

            window.addEventListener("load", function () {
                var circulos=document.querySelectorAll("circle");
            
                for (var c = 0; c < circulos.length; c++) {
                circulos[c].addEventListener("click", ocultar);}
            });

            function ocultar(event) {
                event.target.style.display = "none";
            }
            ;

        </script>
    </head>
    <body>
        <?php

        function calculoColor() {
            $color = "rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")";
            return $color;
        }

        /**
         * Este metodo de colocar las funciones con el codigo HTML escapado
         * no es la mas recomendable, pero para casos de mucho html si

          ?>
          function dibujarCirculo($x, $y) {

          <circle cx ="<?= $x ?>" cy="<?= $y ?>" r ="50" fill="<?= calculoColor() ?>" />

          }
          <?php */

        /**
         * MEJOR ASI:
         */
        function dibujarCirculo($x, $y) {
            //echo "<circle cx = \"$x\" cy = \"$y\" r = \"50\" fill =\"" . calculoColor() . "\" />";
            echo '<circle cx="' . $x . '" cy="' . $y . '" r="50" fill="' . calculoColor() . '" />';
            //Esta forma de ponerlo tambien vale
        }
        ?>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="1000px" height="1000px"
             style="display: block;margin: 0px auto;">
        
            <?php
            dibujarCirculo(50, 50);
                
            dibujarCirculo(100, 50);
                     
            dibujarCirculo(200, 250);
            ?>
              
        </svg>
    </body>
</html>