<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_2</title>
    </head>
    <body>
        <?php
        $longitud = rand(1, 100);
        print "<p>Longitud: $longitud</p>";
        echo "<br>";
        //La expresion "<?=" es para imprimir directamente lo que esta entre sus llaves
        ?>
        <svg width="<?= $longitud ?>px" height="10px">

        <?php
        //Forma alternativa de imprimir
        echo '<line x1="1" y1="5" x2="' . $longitud . '" y2="5" stroke="red" stroke-width="1">';
        /*
         * Los IDES trabajaran mucho mejor si intentas escribir HTML fuera de PHP
         */
        ?>   
        </svg>

    </body>
</html>
