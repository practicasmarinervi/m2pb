<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_5</title>
        <style>
            div {
                margin: 30px auto;
                width: 50px;
                height: 50px;
                border-radius: 50px;
                background-color: red;
            }
        </style>

    </head>
    <body>
        <?php

        function calculoColor() {
            $color = "rgb(" . rand(0, 255) . "," . rand(0, 255) . "," . rand(0, 255) . ")";
            return $color;
        }
        ?>
        <div style="background-color: <?= calculoColor() ?>"></div>
    </body>
</html>
