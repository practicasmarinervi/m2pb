<!DOCTYPE html>
<!--Programa que cada vez que se ejecuta muestra una linea de longitud entre 10 y 1000 pixeles, al azar
Se va a utilizar SVG -->
<html>
    <head>
        <meta charset="UTF-8">
        <title>p1_1</title>
        <?php
        $min = 10;
        $max = 1000;
        $linea = rand($min, $max);
        ?>
        <style>
            div {
                height: 20px;
                width: <?php echo $linea ?>px;
                background-color: red;
            }
        </style>
    </head>

    <body>

        <div></div>
    </body>
</html>
