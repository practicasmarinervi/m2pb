<?php
include "2b_o1.php";
$tirada1=new Tirada(140);
$tirada2=new Tirada(140);
?>

<!DOCTYPE html>
<!--
Programa que cada vez que se ejecuta muestra dos tiradas de dados
entre 1 y 6, al azar, indicando el resultado total. Realizada con objetos php
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>pb2_objetos</title>
        <style>
            .contenedor{
                margin: 10px auto;
                width: <?= ($tirada1->lado*2)+10 ?>px;
            }
            p,h1{
                 text-align: center;
                 
            }    
            span{
                border: 1px solid black;
                font-size: 50px;
                padding: 5px;
            }
        </style>
       
    </head>
    <body>
            <div class="contenedor"  >
            <h1>Tirada de dados</h1>
            <div class="dados">
                <img src="imgs/<?= $tirada1->dado ?>.svg" alt="1" width=<?= $tirada1->lado ?> height=<?= $tirada1->lado ?> />
                <img src="imgs/<?= $tirada2->dado ?>.svg" alt="1" width=<?= $tirada2->lado ?> height=<?= $tirada2->lado ?> />

            </div>
            <p>Total:<span class="resultado"><?= $tirada1->dado + $tirada2->dado ?></span></p>
        </div>
    </body>
</html>



