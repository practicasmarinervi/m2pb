<?php
include "2b_o10.php";
$contenedor=new Tirada(140);
?>

<!DOCTYPE html>
<!--
Programa que simula tirar 10 veces unos dados. En un array bidimensional almacena el resultado de cada
dado en cada tirada-->  
<html>
    <head>
        <meta charset="UTF-8">
        <title>pb2_o10</title>
<style>
            .contenedor{
                margin: 10px auto;
                width: <?= ($contenedor->lado*2)+10 ?>px;
            }
            p,h1{
                 text-align: center;
                 
            }    
            span{
                border: 1px solid black;
                font-size: 50px;
                padding: 5px;
            }
        </style>
       
    </head>
    <body>
        
         <?php
        $tiradas = [];
        $sumaTiradas = [];
        
        for ($c = 0; $c < 10; $c++) {
            $tirada1=new Tirada(140);
            $tirada2=new Tirada(140);
            $tiradas[]=[
                'dado1'=>$tirada1->dado,
                'dado2'=>$tirada2->dado
            ];
                      
            $sumaTiradas[] = $tirada1->dado+ $tirada2->dado;
        }
        
        $mayor=  max($sumaTiradas);
        var_dump($mayor);
        var_dump($sumaTiradas);
        foreach ($sumaTiradas as $i => $v) {
            if($v==$mayor){
                         
            ?>
        <div class="contenedor">
                <h1>De las 10 Tiradas, la de mayor puntuación es:</h1>
                <div class="dados">
                    <img src="imgs/<?= $tiradas[$i]['dado1'] ?>.svg" alt="dado1"/>
                    <img src="imgs/<?= $tiradas[$i]['dado2'] ?>.svg" alt="dado2"/>
                </div>
                <p><span><?= $v ?></span></p>
            </div>
            <?php
        }
         }
           ?>  
    </body>
</html>
