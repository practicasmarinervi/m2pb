<!DOCTYPE html>
<!--
Programa que simula tirar 10 veces unos dados. En un array se guardan la suma de cada una
de las 10 tiradas y se muestra la tirada con puntuacion maxima-->  
<html>
    <head>
        <meta charset="UTF-8">
        <title>pb2_2</title>
    </head>
    <body>
        <?php
        $contador = 0;
        $sumaTiradas = [];

        for ($c = 0; $c < 10; $c++) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
            $sumaTiradas[] = $d1 + $d2;
            ?>
            <div>
                <div class="dados">
                    <img src="imgs/<?= $d1 ?>.svg" alt="dado1"/>
                    <img src="imgs/<?= $d2 ?>.svg" alt="dado2"/>
                </div>
                <div class="total">Total: <span><?= $sumaTiradas[$contador++] ?></span></div>
            </div>
            <?php
        }
        var_dump($sumaTiradas);
        ?>
        <div class="mayor">
            La tirada mayor ha sido de <?= max($sumaTiradas) ?>
        </div>


        <!--Resultado:
     
    array (size=10)
    0 => int 10
    1 => int 5
    2 => int 7
    3 => int 4
    4 => int 10
    5 => int 6
    6 => int 8
    7 => int 10
    8 => int 9
    9 => int 8
  
  La tirada mayor ha sido de 10 
        -->  


    </body>
</html>
