<!DOCTYPE html>
<!--
Programa que cada vez que se ejecuta muestra dos tiradas de dados
entre 1 y 6, al azar, indicando el resultado total
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>pb2_1</title>
       
    </head>
    <body>
        <?php
        $ancho=140;
        $alto=140;
        $dado1 = rand(1, 6);
        $dado2 = rand(1, 6);
        ?>
        <div class="contenedor width=<?= $ancho*2 ?> >
            <h1>Tirada de dados</h1>
            <div class="dados">
                <img src="imgs/<?= $dado1 ?>.svg" alt="1" width=<?= $ancho ?> height=<?= $alto ?> />
                <img src="imgs/<?= $dado2 ?>.svg" alt="2" width=<?= $ancho ?> height=<?= $alto ?> />
            </div>
            <p>Total:<?= $dado1 + $dado2 ?></p>
        </div>
    </body>
</html>



