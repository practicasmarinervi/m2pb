<!DOCTYPE html>
<!--
Programa que simula tirar 10 veces unos dados. En un array bidimensional almacena el resultado de cada
dado en cada tirada-->  
<html>
    <head>
        <meta charset="UTF-8">
        <title>pb2_3</title>
    </head>
    <body>
        <?php
        $tiradas = [];
        $sumaTiradas = [];
        /*
         * Paso de argumentos por referencia:
          Por defecto, los argumentos de las funciones son pasados por valor (así, si el valor del argumento dentro
          de la función cambia, este no cambia fuera de la función). Para permitir a una función modificar sus
          argumentos, éstos deben pasarse por referencia.
          Para hacer que un argumento a una función sea siempre pasado por referencia hay que anteponer al nombre del
          argumento el signo 'et' (&) en la definición de la función
         */

        function tirada(&$d1, &$d2) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
        }

        for ($c = 0; $c < 10; $c++) {
            tirada($tiradas[$c][0], $tiradas[$c][1]);
            $sumaTiradas[] = $tiradas[$c][0] + $tiradas[$c][1];
        }
        foreach ($tiradas as $i => $v) {
            ?>
            <div>
                <div class="dados">
                    <img src="imgs/<?= $v[0] ?>.svg" alt="dado1"/>
                    <img src="imgs/<?= $v[1] ?>.svg" alt="dado2"/>
                </div>
                <div class="total">Total: <span><?= $sumaTiradas[$i] ?></span></div>
            </div>
            <?php
        }
        var_dump($tiradas);
        ?>

        <!--Resultado: Con un array enumerado
     
    array (size=10)
  0 => 
    array (size=2)
      0 => int 2
      1 => int 2
  1 => 
    array (size=2)
      0 => int 4
      1 => int 3
  2 => 
    array (size=2)
      0 => int 5
      1 => int 1
  3 => 
    array (size=2)
      0 => int 5
      1 => int 5
  4 => 
    array (size=2)
      0 => int 1
      1 => int 1
  5 => 
    array (size=2)
      0 => int 5
      1 => int 3
  6 => 
    array (size=2)
      0 => int 5
      1 => int 6
  7 => 
    array (size=2)
      0 => int 6
      1 => int 4
  8 => 
    array (size=2)
      0 => int 6
      1 => int 1
  9 => 
    array (size=2)
      0 => int 3
      1 => int 3
        -->  


    </body>
</html>
