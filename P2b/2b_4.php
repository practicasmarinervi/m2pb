<!DOCTYPE html>
<!--
Programa que simula tirar 10 veces unos dados. En un array bidimensional almacena el resultado de cada
dado en cada tirada-->  
<html>
    <head>
        <meta charset="UTF-8">
        <title>pb2_4</title>
    </head>
    <body>
        <?php
        $tiradas = [];
        $sumaTiradas = [];

        function tirada(&$d1, &$d2) {
            $d1 = rand(1, 6);
            $d2 = rand(1, 6);
        }

        for ($c = 0; $c < 10; $c++) {
            tirada($tiradas[$c]['dado1'], $tiradas[$c]['dado2']);
            $sumaTiradas[] = $tiradas[$c]['dado1'] + $tiradas[$c]['dado2'];
        }
        foreach ($tiradas as $i => $v) {
            ?>
            <div>
                <div class="dados">
                    <img src="imgs/<?= $v['dado1'] ?>.svg" alt="dado1"/>
                    <img src="imgs/<?= $v['dado2'] ?>.svg" alt="dado2"/>
                </div>
                <div class="total">Total: <span><?= $sumaTiradas[$i] ?></span></div>
            </div>
            <?php
        }
        var_dump($tiradas);
        ?>

        <!--Resultado: Con un array asociativo
     
  array (size=10)
  0 => 
    array (size=2)
      'dado1' => int 2
      'dado2' => int 1
  1 => 
    array (size=2)
      'dado1' => int 2
      'dado2' => int 5
  2 => 
    array (size=2)
      'dado1' => int 3
      'dado2' => int 4
  3 => 
    array (size=2)
      'dado1' => int 1
      'dado2' => int 2
  4 => 
    array (size=2)
      'dado1' => int 3
      'dado2' => int 1
  5 => 
    array (size=2)
      'dado1' => int 2
      'dado2' => int 6
  6 => 
    array (size=2)
      'dado1' => int 1
      'dado2' => int 5
  7 => 
    array (size=2)
      'dado1' => int 1
      'dado2' => int 5
  8 => 
    array (size=2)
      'dado1' => int 2
      'dado2' => int 3
  9 => 
    array (size=2)
      'dado1' => int 3
      'dado2' => int 1
        -->  


    </body>
</html>
