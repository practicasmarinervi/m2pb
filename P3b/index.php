<?php

//Utilizar variables de sesion para almacenar la tirada de un dado
session_start();
if (!isset($_SESSION["tirada"])){
    // Creando e inicializando la variable de sesion
    $_SESSION["tirada"] = [];
    $_SESSION['contador']=0;
}
include 'p3.php';
?> 

<!DOCTYPE html>
<!--
Programa que cada vez que se ejecuta muestra una tirada de dados entre 1 y 6 al azar
Se coloca una ficha sobre un tablero en la posicion correspondiente al resultado del dado.
Almacenar cada resultado en una variable de sesion-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>p3b_index</title>
        <style>
            .contenedor{
                margin: 10px auto;
                width: 100%;
            }
            .tablero{
                width: 90%;
                height: 100%;
            }
            form input{
                padding: 10px;
                border-top: 20px;
            }
        </style>
    </head>
    <body>
        <div class=contenedor >
             <h1>Tirada de dado con tablero de resultado</h1>
             <div class=dado>
             <img src="imgs/<?= $dado ?>.svg" alt="1" width=<?= $ancho ?> height=<?= $alto ?> />
             </div> 
            
             <div class="tablero">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"accesskey=""width="620" height="120" viewBox="-15 -15 620 120" style="background-color: white;" font-family="sans-serif">;
        <rect x="0" y="0" width="100" height="100" stroke="black" stroke-width="1" fill="none" />;
        <text x="50" y="80" text-anchor="middle" font-size="80" fill="lightgray" >1</text>;
        <rect x="100" y="0" width="100" height="100" stroke="black" stroke-width="1" fill="none" />;
        <text x="150" y="80" text-anchor="middle" font-size="80" fill="lightgray" >2</text>;
        <rect x="200" y="0" width="100" height="100" stroke="black" stroke-width="1" fill="none" />;
        <text x="250" y="80" text-anchor="middle" font-size="80" fill="lightgray" >3</text>;
        <rect x="300" y="0" width="100" height="100" stroke="black" stroke-width="1" fill="none" />;
        <text x="350" y="80" text-anchor="middle" font-size="80" fill="lightgray" >4</text>;
        <rect x="400" y="0" width="100" height="100" stroke="black" stroke-width="1" fill="none" />;
        <text x="450" y="80" text-anchor="middle" font-size="80" fill="lightgray" >5</text>;
        <rect x="500" y="0" width="100" height="100" stroke="black" stroke-width="1" fill="none" />;
        <text x="550" y="80" text-anchor="middle" font-size="80" fill="lightgray" >6</text>;
        <circle cx="<?= (100*$dado)-50 ?>" cy="50" r="30" stroke="black" stroke-width="2" fill="red" />;
        </svg> 
      
        <form method="get" >
            <input type="submit" name='limpiar' value="Limpiar" />
             <input type="submit" name='tirar' value="Tirar" />
        </form>
       </div> 
     </div>     
             
   </body>
</html>
     